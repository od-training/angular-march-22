import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { filter, map, Observable, ReplaySubject } from 'rxjs';

@Component({
  selector: 'mar-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit, OnChanges {
  @Input() videoId = '';
  private _videoId = new ReplaySubject<string>(1);
  videoUrl: Observable<SafeUrl>;

  constructor(sanitizer: DomSanitizer) {
    this.videoUrl = this._videoId.pipe(
      filter(id => !!id),
      map(id => sanitizer.bypassSecurityTrustResourceUrl('https://youtube.com/embed/' + id))
    );
  }

  ngOnChanges() {
    this._videoId.next(this.videoId);
  }

  ngOnInit(): void {
  }

}
