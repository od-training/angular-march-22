import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable } from 'rxjs';
import { VideoLoaderService } from '../../api/video-loader.service';
import { Video } from '../../types';

@Component({
  selector: 'mar-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {
  videoList: Observable<Video[]>;
  selectedVideoId: Observable<string>;

  constructor(vls: VideoLoaderService, route: ActivatedRoute) {
    this.videoList = vls.filteredVideos;

    this.selectedVideoId = route.queryParamMap.pipe(
      map(params => params.get('id') || '')
    );
  }

  ngOnInit(): void {
  }

}
