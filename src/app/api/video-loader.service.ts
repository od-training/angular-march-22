import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map, combineLatest, BehaviorSubject } from 'rxjs';
import { Video } from '../types';

export interface FilterCriteria {
  title?: string;
  author?: string;
}

const videosUrl = 'https://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root',
})
export class VideoLoaderService {
  private filters = new BehaviorSubject<FilterCriteria>({});

  filteredVideos = combineLatest([this.loadVideos(), this.filters]).pipe(
    map(([videos, filters]) => filterVideos(videos, filters)),
    map(obfuscateAuthorNames)
  );

  constructor(private http: HttpClient) {}

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(videosUrl);
  }

  loadVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`${videosUrl}/${id}`);
  }

  setFilters(newFilter: FilterCriteria) {
    this.filters.next(newFilter);
  }
}

function filterVideos(videos: Video[], filters: FilterCriteria): Video[] {
  return videos.filter((video) => {
    return (
      (!filters.title || video.title.indexOf(filters.title) > -1) &&
      (!filters.author || video.author.indexOf(filters.author) > -1)
    );
  });
}

function obfuscateAuthorNames(videos: Video[]): Video[] {
  return videos.map((video) => {
    const author = video.author.slice(1) + video.author[0] + 'ay';
    return { ...video, author };
  });
}
