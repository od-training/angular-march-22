import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Video } from '../../types';

@Component({
  selector: 'mar-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[] = [];
  @Input() selectedVideoId = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  selectVideo(id: string) {
    const queryParams = { id };
    this.router.navigate([], { queryParams });
  }

}

