import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { VideoLoaderService } from '../../api/video-loader.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'mar-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit, OnDestroy {
  filters: FormGroup;
  private destroy = new Subject<void>();

  constructor(fb: FormBuilder, vls: VideoLoaderService) {
    this.filters = fb.group({
      title: [''],
      author: ['']
    });

    this.filters.valueChanges.pipe(
      takeUntil(this.destroy)
    ).subscribe(val => vls.setFilters(val))
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

}
