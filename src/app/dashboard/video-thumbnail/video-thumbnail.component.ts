import { Component, Input, OnInit } from '@angular/core';
import { Video } from '../../types';

@Component({
  selector: 'mar-video-thumbnail',
  templateUrl: './video-thumbnail.component.html',
  styleUrls: ['./video-thumbnail.component.scss']
})
export class VideoThumbnailComponent implements OnInit {
  @Input() video: Video | undefined;
  @Input() selected = false;

  constructor() { }

  ngOnInit(): void {
  }

}
